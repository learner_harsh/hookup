//
//  RatingVC.swift
//  Hookup
//
//  Created by MacBook on 7/25/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController {
    
    static func create(requestId: String) -> FeedbackVC {
        let ratingVC = FeedbackVC(nibName: "RatingVC", bundle: nil)
        ratingVC.viewModel = FeedbackVCVM(requestId: requestId, view: ratingVC)
        return ratingVC
    }

    @IBOutlet weak var tblView: UITableView! {
        didSet {
            tblView.register(UINib(nibName: UserFeedbackCell.identifier, bundle: nil), forCellReuseIdentifier: UserFeedbackCell.identifier)
            tblView.register(UINib(nibName: NewFeedbackCell.identifier, bundle: nil), forCellReuseIdentifier: NewFeedbackCell.identifier)
            tblView.delegate = self
            tblView.dataSource = self
        }
    }
    
    var viewModel: FeedbackVCVM!
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    deinit {
        debugPrint("deinit RatingVC")
    }
}

extension FeedbackVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.componentCount
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let component = viewModel.getComponent(section)
        return viewModel.getViewCount(component: component)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let component = viewModel.getComponent(indexPath.section)
        switch component {
        case .otherFeedback:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: UserFeedbackCell.identifier, for: indexPath) as? UserFeedbackCell else {return UITableViewCell()}
            viewModel.showOtherFeedbackCell(cell)
            return cell
        case .myFeedback:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: UserFeedbackCell.identifier, for: indexPath) as? UserFeedbackCell else {return UITableViewCell()}
            viewModel.showMyFeedbackCell(cell)
            return cell
        case .newFeedback:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NewFeedbackCell.identifier, for: indexPath) as? NewFeedbackCell else {return UITableViewCell()}
            viewModel.showNewFeedbackCell(cell)
            return cell
        }
    }

}

extension FeedbackVC: FeedbackVCView {
    func dismiss() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func refreshView() {
        tblView.reloadData()
    }
}

extension FeedbackVC {
    private func setNavigation() {
        let yourBackImage = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = yourBackImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        navigationController?.navigationBar.backItem?.title = ""
        navigationController?.navigationBar.tintColor = .black
        navigationItem.title = "Feedback"
    }
}
