//
//  RatingVCVM.swift
//  Hookup
//
//  Created by MacBook on 7/25/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import Foundation
enum RatingViewComponent {
    case otherFeedback
    case myFeedback
    case newFeedback
}

protocol FeedbackVCView: AnyObject {
    func refreshView()
    func dismiss()
}

protocol FeedbackVCVMAction {
    func viewDidLoad()
}

protocol FeedbackVCVMInput {
    var componentCount: Int { get }
    func getComponent(_ index: Int) -> RatingViewComponent
    func getViewCount(component: RatingViewComponent) -> Int
    func showNewFeedbackCell(_ cell: NewFeedbackCellProtocol)
    func showMyFeedbackCell(_ cell: UserFeedbackCell)
    func showOtherFeedbackCell(_ cell: UserFeedbackCell)
}

class FeedbackVCVM {
    private var requestID: String
    private weak var view: FeedbackVCView?
    private var components: [RatingViewComponent] = [.myFeedback, .newFeedback, .otherFeedback]
    private var feedbacks: [Feedback] = []
    private var isDataLoad: Bool = false
    init(requestId: String, view: FeedbackVCView) {
        requestID = requestId
        self.view = view
    }
    
    deinit {
        debugPrint("Deinit RatingVCVM")
    }
}

extension FeedbackVCVM: FeedbackVCVMAction {
    func viewDidLoad() {
        let param: [String: Any] = ["feedback_id": requestID]
        NetworkServices.manager.getFeedback(params: param) { [weak self] requestedData in
            guard let self = self else {return}
            self.isDataLoad = true
            self.feedbacks = requestedData?.feedbacks ?? []
            self.view?.refreshView()
        } onCompletion: {

        }
    }
    
    private func otherFeedback() -> Feedback? {
        var otherFeedback: Feedback?
        feedbacks.forEach { [weak self] feedback in
            guard let _ = self else {return}
            if feedback.myFeedback == false {
                otherFeedback = feedback
            }
        }
        return otherFeedback
    }
    
    private func myFeedback() -> Feedback? {
        var myFeedback: Feedback?
        feedbacks.forEach { [weak self] feedback in
            guard let _ = self else {return}
            if feedback.myFeedback == true {
                myFeedback = feedback
            }
        }
        return myFeedback
    }
    
    private func showFeedbackCell(_ cell: UserFeedbackCell, _ feedback: Feedback) {
        var userName: String = ""
        if let firstName = feedback.firstName, !firstName.isEmpty {
            userName += firstName
        }
        if let lastName = feedback.lastName, !lastName.isEmpty {
            if userName.isEmpty {
                userName += lastName
            } else {
                userName += (" " + lastName)
            }
        }
        if feedback.myFeedback == true {
            userName = "My Feedback"
        }
        cell.showData(viewModel: UserFeedbackCellVM(title: userName, rating: Int(feedback.rating ?? "1") ?? 1, feeback: feedback.feedbackText ?? ""))
    }
}

extension FeedbackVCVM: FeedbackVCVMInput {
    
    func showMyFeedbackCell(_ cell: UserFeedbackCell) {
        guard let feedback = myFeedback() else { return }
        showFeedbackCell(cell, feedback)
    }
    
    func showOtherFeedbackCell(_ cell: UserFeedbackCell) {
        guard let feedback = otherFeedback() else { return }
        showFeedbackCell(cell, feedback)
    }
    
    func showNewFeedbackCell(_ cell: NewFeedbackCellProtocol) {
        cell.showData(viewModel: NewFeedbackCellVM(ratingTitle: "Your Rating", feebackTitle: "Share your experience"), delegate: self)
    }
    
    func getComponent(_ index: Int) -> RatingViewComponent {
        return components[index]
    }
    
    func getViewCount(component: RatingViewComponent) -> Int {
        if !self.isDataLoad {
            return 0
        }
        switch component {
        case .otherFeedback:
            return otherFeedback() != nil ? 1 : 0
        case .myFeedback:
            return myFeedback() != nil ? 1 : 0
        case .newFeedback:
            return myFeedback() != nil ? 0 : 1
        }
    }
    
    var componentCount: Int {
        return components.count
    }
}

extension FeedbackVCVM: NewFeedbackCellDelegate {
    func feedback(_ rating: Int, feedback: String) {
        let param: [String: Any] = ["ratings":rating,"feedback_text":feedback,"request_id":requestID]
        NetworkServices.manager.saveFeedback(params: param) { [weak self] requestedData in
            guard let self = self else {return}
            self.view?.dismiss()
        } onCompletion: {

        }
    }
}

