//
//  FeedbackCell.swift
//  Hookup
//
//  Created by MacBook on 7/25/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit
import AARatingBar

protocol UserFeedbackCellProtocol {
    func showData(viewModel: UserFeedbackCellVMInput)
}

class UserFeedbackCell: UITableViewCell {
    static let identifier = "UserFeedbackCell"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewRating: AARatingBar!
    @IBOutlet weak var lblFeedback: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    deinit {
        debugPrint("deinit UserFeedbackCell")
    }
    
}

// MARK: View Model Binding
extension UserFeedbackCell {
    private func bind(viewModel: UserFeedbackCellVMInput) {
        viewModel.title.observe(on: self) {[weak self] in self?.updateTitle($0)}
        viewModel.rating.observe(on: self) {[weak self] in self?.updateRating($0)}
        viewModel.feeback.observe(on: self) {[weak self] in self?.updateFeedback($0)}
    }
    
    private func updateTitle(_ title: String) {
        lblTitle.text = title
    }
    
    private func updateFeedback(_ title: String) {
        lblFeedback.text = title
    }
    
    private func updateRating(_ rating: Int) {
        viewRating.value = CGFloat(rating)
    }
}

extension UserFeedbackCell: UserFeedbackCellProtocol {
    func showData(viewModel: UserFeedbackCellVMInput) {
        bind(viewModel: viewModel)
    }
}


