//
//  UserFeedbackCellVM.swift
//  Hookup
//
//  Created by MacBook on 7/25/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import Foundation

protocol UserFeedbackCellVMInput {
    var title: Observable<String> {get}
    var rating: Observable<Int> {get}
    var feeback: Observable<String> {get}
}

class UserFeedbackCellVM: UserFeedbackCellVMInput {
    var title: Observable<String> = Observable("")
    var rating: Observable<Int> = Observable(1)
    var feeback: Observable<String> = Observable("")
    init(title: String, rating: Int, feeback: String) {
        self.title.value = title
        self.rating.value = rating
        self.feeback.value = feeback
    }
    
    deinit {
        debugPrint("deinit UserFeedbackCellVM")
    }
}
