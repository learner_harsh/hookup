//
//  NewFeedbackCell.swift
//  Hookup
//
//  Created by MacBook on 7/26/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit
import AARatingBar

protocol NewFeedbackCellDelegate: AnyObject {
    func feedback(_ rating: Int, feedback: String)
}

protocol NewFeedbackCellProtocol {
    func showData(viewModel: NewFeedbackCellVM, delegate: NewFeedbackCellDelegate?)
}

class NewFeedbackCell: UITableViewCell {
    static let identifier = "NewFeedbackCell"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewRating: AARatingBar!
    @IBOutlet weak var lblFeedbackTitle: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    private weak var delegate: NewFeedbackCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    deinit {
        debugPrint("deinit NewFeedbackCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        guard !txtView.text.isEmpty else { return }
        delegate?.feedback(Int(viewRating.value), feedback: txtView.text)
    }
}

extension NewFeedbackCell {
    private func setupUI() {
        txtView.border(withRadius: 5, borderWidth: 1, borderColor: .lightGray)
        btnSubmit.roundCorners(.allCorners, radius: 5)
    }
}

// MARK: View Model Binding
extension NewFeedbackCell {
    private func bind(viewModel: NewFeedbackCellVM) {
        viewModel.ratingTitle.observe(on: self) {[weak self] in self?.updateRatingTitle($0)}
        viewModel.feebackTitle.observe(on: self) {[weak self] in self?.updateFeedbackTitle($0)}
    }
    
    private func updateRatingTitle(_ title: String) {
        lblTitle.text = title
    }
    
    private func updateFeedbackTitle(_ title: String) {
        lblFeedbackTitle.text = title
    }
}

extension NewFeedbackCell: NewFeedbackCellProtocol {
    func showData(viewModel: NewFeedbackCellVM, delegate: NewFeedbackCellDelegate?) {
        self.delegate = delegate
        bind(viewModel: viewModel)
    }
}

