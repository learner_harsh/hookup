//
//  NewFeedbackCellVM.swift
//  Hookup
//
//  Created by MacBook on 7/26/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import Foundation
protocol NewFeedbackCellVMInput {
    var ratingTitle: Observable<String> {get}
    var feebackTitle: Observable<String> {get}
}

class NewFeedbackCellVM: NewFeedbackCellVMInput {
    var ratingTitle: Observable<String> = Observable("")
    var feebackTitle: Observable<String> = Observable("")
    init(ratingTitle: String, feebackTitle: String) {
        self.ratingTitle.value = ratingTitle
        self.feebackTitle.value = feebackTitle
    }
    
    deinit {
        debugPrint("deinit NewFeedbackCellVM")
    }
}
