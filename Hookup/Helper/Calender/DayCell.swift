//
//  Created by Dmitry Ivanenko on 01.10.16.
//  Copyright © 2016 Dmitry Ivanenko. All rights reserved.
//

import UIKit


open class DayCell: UICollectionViewCell {

    @IBOutlet public weak var dateLabel: UILabel!
    @IBOutlet public weak var weekDayLabel: UILabel!
    @IBOutlet public weak var monthLabel: UILabel!
    @IBOutlet public weak var selectorView: UIView!

    static var ClassName: String {
        return String(describing: self)
    }

    // MARK: - Setup

    func setup(date: Date, isSelected: Bool) {
        let formatter = DateFormatter()

        formatter.dateFormat = "dd"
        dateLabel.text = formatter.string(from: date)
        dateLabel.textColor = isSelected ? .black : .white

        formatter.dateFormat = "EEE"
        weekDayLabel.text = formatter.string(from: date).uppercased()
        weekDayLabel.textColor = isSelected ? .black : .white

        formatter.dateFormat = "MMMM"
        monthLabel.text = formatter.string(from: date).uppercased()
        monthLabel.textColor = isSelected ? .black : .white

        selectorView.backgroundColor = isSelected ? .red : .clear
//        backgroundColor = style.backgroundColor ?? backgroundColor
        
        if date > Date() || Calendar.current.isDate(date, inSameDayAs: Date()) {
            dateLabel.alpha = 1.0
            weekDayLabel.alpha = 1.0
            monthLabel.alpha = 1.0
        } else {
            dateLabel.alpha = 0.3
            weekDayLabel.alpha = 0.3
            monthLabel.alpha = 0.3
        }
    }
}

