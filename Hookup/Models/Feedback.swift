//
//  Feedback.swift
//  Hookup
//
//  Created by MacBook on 7/25/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import Foundation
struct Feedback: Codable {
    
    let feedbackText: String?
    let rating: String?
    let userId: String?
    let firstName: String?
    let lastName: String?
    let myFeedback: Bool?
    
    enum CodingKeys: String, CodingKey {
        case feedbackText = "feedback_text"
        case rating = "feedback_rating"
        case userId = "user_id"
        case firstName = "first_name"
        case lastName = "last_name"
        case myFeedback = "my_feedback"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        feedbackText = try values.decodeIfPresent(String.self, forKey: .feedbackText)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        myFeedback = try values.decodeIfPresent(Bool.self, forKey: .myFeedback)
    }
}
