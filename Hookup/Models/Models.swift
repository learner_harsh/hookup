//
//  Models.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Foundation

struct ResponseModel : Codable {

    let response : Response?
    let status : String?


    enum CodingKeys: String, CodingKey {
        case response = "response"
        case status = "status"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let responsee = try values.decodeIfPresent([Response].self, forKey: .response) {
            response = responsee.first
        } else {
            response = try values.decodeIfPresent(Response.self, forKey: .response)
        }
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }


}

struct Response : Codable {

    let code : Int?
    let data: Dataa?
    let msg : String?
    let key : String?


    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data = "data"
        case msg = "msg"
        case key = "key"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        data = try values.decodeIfPresent(Dataa.self, forKey: .data)
        msg = try values.decodeIfPresent(String.self, forKey: .msg)
        key = try values.decodeIfPresent(String.self, forKey: .key)
    }


}


struct Dataa : Codable {

    let userData : Users?
    let requestData : [RequestItems]?
    let profileData: ProfileData?
    let usersListData: [ProfileData]?
    let message: String?
    let requestUserData : [RequestItems]?
    let requestReceived : [RequestsSent]?
    let requestsSent : [RequestsSent]?
    let feedbacks : [Feedback]?
    

    enum CodingKeys: String, CodingKey {
        case userData = "user_data"
        case requestData = "request_items"
        case profileData = "profile_data"
        case usersListData = "users"
        case message = "msg"
        case requestUserData = "user_request_items"
        case requestReceived = "request_received"
        case requestsSent = "requests_sent"
        case feedbacks = "feedbacks"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userData = try values.decodeIfPresent(Users.self, forKey: .userData)
        requestData = try values.decodeIfPresent([RequestItems].self, forKey: .requestData)
        profileData = try values.decodeIfPresent(ProfileData.self, forKey: .profileData)
        usersListData = try values.decodeIfPresent([ProfileData].self, forKey: .usersListData)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        requestUserData = try values.decodeIfPresent([RequestItems].self, forKey: .requestUserData)
        requestReceived = try values.decodeIfPresent([RequestsSent].self, forKey: .requestReceived)
        requestsSent = try values.decodeIfPresent([RequestsSent].self, forKey: .requestsSent)
        feedbacks = try values.decodeIfPresent([Feedback].self, forKey: .feedbacks)
    }


}

struct Users : Codable {

    let active : String?
    let authToken : String?
    let contactNumber : String?
    let createdOn : String?
    let deletedOn : String?
    let email : String?
    let firstName : String?
    let id : String?
    let isFirstLogin : Int?
    let lastName : String?
    let location : String?
    let postcode : String?
    let updatedOn : String?
    let gender: String
    let dob: String


    enum CodingKeys: String, CodingKey {
        case active = "active"
        case authToken = "auth_token"
        case contactNumber = "contact_number"
        case createdOn = "created_on"
        case deletedOn = "deleted_on"
        case email = "email"
        case firstName = "first_name"
        case id = "id"
        case isFirstLogin = "is_first_login"
        case lastName = "last_name"
        case location = "location"
        case postcode = "postcode"
        case updatedOn = "updated_on"
        case gender = "gender"
        case dob = "dob"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        active = try values.decodeIfPresent(String.self, forKey: .active)
        authToken = try values.decodeIfPresent(String.self, forKey: .authToken)
        contactNumber = try values.decodeIfPresent(String.self, forKey: .contactNumber)
        createdOn = try values.decodeIfPresent(String.self, forKey: .createdOn)
        deletedOn = try values.decodeIfPresent(String.self, forKey: .deletedOn)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        isFirstLogin = try values.decodeIfPresent(Int.self, forKey: .isFirstLogin)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        postcode = try values.decodeIfPresent(String.self, forKey: .postcode)
        updatedOn = try values.decodeIfPresent(String.self, forKey: .updatedOn)
        gender = try values.decodeIfPresent(String.self, forKey: .gender) ?? "1"
        dob = try values.decodeIfPresent(String.self, forKey: .dob) ?? ""
    }


}

struct RequestItems : Codable {

    var checked : Bool?
    let id : String?
    let itemDescription : String?
    let itemHeading : String?


    enum CodingKeys: String, CodingKey {
        case checked = "checked"
        case id = "id"
        case itemDescription = "item_description"
        case itemHeading = "item_heading"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        checked = try values.decodeIfPresent(Bool.self, forKey: .checked)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        itemDescription = try values.decodeIfPresent(String.self, forKey: .itemDescription)
        itemHeading = try values.decodeIfPresent(String.self, forKey: .itemHeading)
    }


}

struct ProfileData : Codable {

    let dob : String?
    let firstName : String?
    let gender : String?
    let id : String?
    let lastName : String?
    let location : String?
    let postcode : String?
    let profileImgs : [ProfileImg]?
    let profileImg: String?
    let ratings: Int?
    

    enum CodingKeys: String, CodingKey {
        case dob = "dob"
        case firstName = "first_name"
        case gender = "gender"
        case id = "id"
        case lastName = "last_name"
        case location = "location"
        case postcode = "postcode"
        case profileImgs = "profile_imgs"
        case profileImg = "profile_img"
        case ratings = "ratings"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        postcode = try values.decodeIfPresent(String.self, forKey: .postcode)
        profileImgs = try values.decodeIfPresent([ProfileImg].self, forKey: .profileImgs)
        profileImg = try values.decodeIfPresent(String.self, forKey: .profileImg)
        ratings = try values.decodeIfPresent(Int.self, forKey: .ratings)
    }


}

struct ProfileImg : Codable {

    let id : String?
    let imageName : String?
    let isProfilePic : String?


    enum CodingKeys: String, CodingKey {
        case id = "id"
        case imageName = "image_name"
        case isProfilePic = "is_profile_pic"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        imageName = try values.decodeIfPresent(String.self, forKey: .imageName)
        isProfilePic = try values.decodeIfPresent(String.self, forKey: .isProfilePic)
    }


}

struct RequestsSent : Codable {

    let amount : String?
    let message : String?
    let requestId : String?
    let status : String?
    let userName : String?
    let feedbackOption : Bool?


    enum CodingKeys: String, CodingKey {
        case amount = "amount"
        case message = "message"
        case requestId = "request_id"
        case status = "status"
        case userName = "user_name"
        case feedbackOption = "feedback_option"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        amount = try values.decodeIfPresent(String.self, forKey: .amount)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        requestId = try values.decodeIfPresent(String.self, forKey: .requestId)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        feedbackOption = try values.decodeIfPresent(Bool.self, forKey: .feedbackOption)
    }


}


// Errorr ===================

struct ErrorModel : Codable {

    let response : [ResponseError]?
    let status : String?


    enum CodingKeys: String, CodingKey {
        case response = "response"
        case status = "status"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        response = try values.decodeIfPresent([ResponseError].self, forKey: .response)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//    }


}

struct ResponseError : Codable {

    let code : Int?
    let key : String?
    let msg : String?


    enum CodingKeys: String, CodingKey {
        case code = "code"
        case key = "key"
        case msg = "msg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        code = try values.decodeIfPresent(Int.self, forKey: .code)
//        key = try values.decodeIfPresent(String.self, forKey: .key)
//        msg = try values.decodeIfPresent(String.self, forKey: .msg)
//    }


}


enum StatusCode {
    static let ok = "ok"
}


