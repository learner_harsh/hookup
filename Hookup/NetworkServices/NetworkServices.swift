//
//  NetworkServices.swift
//  Buzz
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import SVProgressHUD

class NetworkServices {
    
    static let manager = NetworkServices()
    
    func apiCall(_ api: HookupAPI, onSuccess: @escaping (ResponseModel?) -> Void, onCompletion: @escaping () -> Void) {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.black)
        Network.request(api, success: { (userList: ResponseModel) in
            let response = userList.response
            if userList.status == StatusCode.ok {
                onSuccess(userList)
            } else {
                showAlert(title: "\(response?.code ?? 400)", message: response?.msg ?? "")
            }
        }, error: { (error) in
            print(error)
            let erroDesc = error.response?.first
            showAlert(title: "\(erroDesc?.code ?? 400)", message: erroDesc?.msg ?? "")
        }, failure: { (failurer) in
            print(failurer.errorDescription!)
            showAlert(title: "Opps!", message: failurer.errorDescription!)
        }) {
            print("Completionnn Status")
            SVProgressHUD.dismiss()
            onCompletion()
        }
    }
    
    func checkLogin(params: [String: Any], onSuccess: @escaping (Users?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.checkLogin(params: params), onSuccess: { (responsee) in
            StorageManager.shared.authToken = responsee?.response?.data?.userData?.authToken ?? ""
            onSuccess(responsee?.response?.data?.userData)
        }, onCompletion: onCompletion)
    }
    
    func signUp(params: [String: Any], onSuccess: @escaping (Users?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.register(params: params), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data?.userData)
        }, onCompletion: onCompletion)
    }
    
    func getUsers(onSuccess: @escaping ([ProfileData]?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.getUsers, onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data?.usersListData)
        }, onCompletion: onCompletion)
    }
    
    func getRequestItems(onSuccess: @escaping ([RequestItems]?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.getRequestItems, onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data?.requestData)
        }, onCompletion: onCompletion)
    }
    
    func saveRequestItems(itemids: String, onSuccess: @escaping (String?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.saveRequestItems(itemIds: itemids), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.msg)
        }, onCompletion: onCompletion)
    }
    
    func getProfileData(userId: String, onSuccess: @escaping (ProfileData?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.getProfileInfo(userId: userId), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data?.profileData)
        }, onCompletion: onCompletion)
    }
    
    func saveProfileData(params: [String: Any], onSuccess: @escaping (String?) -> Void, onCompletion: @escaping () -> Void) {
        var param = params
        param["auth_token"] = StorageManager.shared.authToken
        apiCall(.saveProfileInfo(params: param), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data?.message)
        }, onCompletion: onCompletion)
    }
    
    func getUserRequestItems(userId: String, onSuccess: @escaping ([RequestItems]?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.getUserRequestItems(userId: userId), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data?.requestUserData)
        }, onCompletion: onCompletion)
    }
    
    func sendRequestToUser(params: [String: Any], onSuccess: @escaping (String?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.sendRequestToUser(params: params), onSuccess: { (responsee) in
            showAlert(title: "Success!", message: responsee?.response?.msg ?? "")
            onSuccess(responsee?.response?.msg)
        }, onCompletion: onCompletion)
    }
    
    func getMyRequests(onSuccess: @escaping (Dataa?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.getMyRequests, onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data)
        }, onCompletion: onCompletion)
    }
    
    func acceptRejectReq(params: [String: Any], onSuccess: @escaping (String?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.acceptRejectReq(params: params), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.msg)
        }, onCompletion: onCompletion)
    }
    
    func getFeedback(params: [String: Any], onSuccess: @escaping (Dataa?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.getFeedback(params: params), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data)
        }, onCompletion: onCompletion)
    }
    
    func saveFeedback(params: [String: Any], onSuccess: @escaping (Dataa?) -> Void, onCompletion: @escaping () -> Void) {
        apiCall(.saveFeedback(params: params), onSuccess: { (responsee) in
            onSuccess(responsee?.response?.data)
        }, onCompletion: onCompletion)
    }
}
