//
//  BrowseViewController.swift
//  Hookup
//
//  Created by Mayur Boghani on 12/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit
import SDWebImage

class BrowseViewController: UIViewController {
    
    @IBOutlet weak var collectionUserimages: UICollectionView! {
        didSet {
            collectionUserimages.delegate = self
            collectionUserimages.dataSource = self
            collectionUserimages.register(UINib(nibName: UserImageCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: UserImageCollectionViewCell.identifier)
        }
    }
    @IBOutlet weak var viewSuperCollection: UIView!
    @IBOutlet weak var sendReqView: UIVisualEffectView!
    @IBOutlet weak var sendReqStkView: UIStackView!
    @IBOutlet weak var sendReqDismissView: UIView!
    @IBOutlet weak var btnSendRequestBlur: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var lblNameAge: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnSendRequest: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
    }
    var userid = ""
    var userImages = [String]() {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.collectionUserimages.reloadData()
            }
        }
    }
    var userDetail: ProfileData? {
        didSet {
            userImages = userDetail?.profileImgs?.compactMap({$0.imageName}) ?? []
            self.setupData()
        }
    }
    var requestedData = [RequestItems]()
    var requestImgViews = [UIImageView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        getDataFromAPI()
    }
    func getDataFromAPI() {
        NetworkServices.manager.getProfileData(userId: userid, onSuccess: { (profiledata) in
            self.userDetail = profiledata
        }, onCompletion: {
        })
    }
    
    func getRequestItemsAPI() {
        NetworkServices.manager.getUserRequestItems(userId: userid, onSuccess: { (requestedData) in
            self.requestedData = requestedData ?? []
            DispatchQueue.main.async {
                self.showReqView()
            }
        }, onCompletion: {
        })
    }
    
    func showReqView() {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut) {
            self.sendReqView.alpha = 1.0
        } completion: { (_) in
            self.requestedItemsSheet()
        }
    }
    
    func hideReqView() {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseIn) {
            self.sendReqView.alpha = 0.0
        } completion: { (_) in
        }
    }
    
    func sendRequest() {
        if validated() {
            let reqIds = requestedData.filter({$0.checked == true})
                .compactMap({$0.id})
                .joined(separator: ",")
            let param = ["request_items_ids": reqIds,
                         "amount": txtAmount.text ?? "",
                         "message": txtMessage.text ?? ""] as [String : Any]
            NetworkServices.manager.sendRequestToUser(params: param, onSuccess: { (requestedData) in
                print("Send Requested Data == ", requestedData)
                self.hideReqView()
            }, onCompletion: {
                
            })
        }
    }
    
    func setupLayout() {
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        //        if let headerview = Bundle.main.loadNibNamed(HeaderMenuView.identifier, owner: nil, options: nil)?.first as? HeaderMenuView {
        //            headerview.lblTitle.text = Constant.browse.localized
        //            headerview.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeader.bounds.height)
        //            viewHeader.addSubview(headerview)
        //        }
        
        viewSuperCollection.layer.cornerRadius = 12
        viewSuperCollection.addShadow()
        lblNameAge.font = UIFont.lato(ofSize: 24, weight: .bold)
        lblAddress.font = UIFont.lato(ofSize: 16, weight: .light)
        lblAddress.textColor = ThemeColor.textColor
        lblRating.font = UIFont.lato(ofSize: 16, weight: .light)
        lblRating.textColor = ThemeColor.textColor
        lblNameAge.textColor = ThemeColor.textColor
        
        btnSendRequest.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        btnSendRequest.setTitle(Constant.sendReq.localized, for: .normal)
        btnSendRequest.addTarget(self, action: #selector(didTapSendRequest(_:)), for: .touchUpInside)
        btnLike.addTarget(self, action: #selector(didTapLike(_:)), for: .touchUpInside)
        btnDislike.addTarget(self, action: #selector(didTapDislike(_:)), for: .touchUpInside)
        
        lblAmount.text = Constant.amount.localized.uppercased()
        lblMessage.text = Constant.message.localized.uppercased()
        
        lblAmount.font = UIFont.lato(ofSize: 16, weight: .light)
        lblMessage.font = UIFont.lato(ofSize: 16, weight: .light)
        txtAmount.font = UIFont.lato(ofSize: 14, weight: .light)
        txtMessage.font = UIFont.lato(ofSize: 14, weight: .light)
        
        lblAmount.textColor = ThemeColor.textColor
        lblMessage.textColor = ThemeColor.textColor
        txtAmount.textColor = ThemeColor.textColor
        txtMessage.textColor = ThemeColor.textColor
        
        btnSendRequestBlur.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        btnSendRequestBlur.setTitle(Constant.sendReq.localized, for: .normal)
        btnSendRequestBlur.addTarget(self, action: #selector(didTapSendRequest(_:)), for: .touchUpInside)
        
        let tapp = UITapGestureRecognizer(target: self, action: #selector(didTapDismissReq(_:)))
        sendReqDismissView.addGestureRecognizer(tapp)
        btnChat.addTarget(self, action: #selector(didTapChat(_:)), for: .touchUpInside)
    }
    
    func setupData() {
        pageControl.numberOfPages = userImages.count
        guard let userDetail = userDetail else { return }
        lblNameAge.text = "\(userDetail.firstName ?? "") \(userDetail.lastName ?? ""), \(userDetail.dob?.age ?? "")"
        lblAddress.text = userDetail.location
        guard let ratings = userDetail.ratings else { return     }
        lblRating.text = "\(ratings)"
    }
    
    @objc func didTapSendRequest(_ sender: UIButton) {
        //        let vc = storyboard?.instantiateViewController(withIdentifier: "ServicesSettingsViewController") as! ServicesSettingsViewController
        //        self.navigationController?.pushViewController(vc, animated: true)
        if sender == btnSendRequest {
            getRequestItemsAPI()
        } else {
            sendRequest()
        }
    }
    
    @objc func didTapChat(_ sender: UIButton) {
        guard let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC else {return}
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @objc func didTapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapSelectReq(_ sender: UITapGestureRecognizer) {
        let tag = Int(sender.name ?? "0") ?? 0
        var checked = requestedData[tag].checked ?? false
        checked.toggle()
        requestedData[tag].checked = checked
        requestImgViews[tag].image = UIImage(named: checked ? "ic_check" : "ic_uncheck")
    }
    
    @objc func didTapLike(_ sender: UIButton) {
//        guard let editProfileVC = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController else {return}
//        editProfileVC.userDetails = userDetail
//        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
    @objc func didTapDislike(_ sender: UIButton) {
        
    }
    
    @objc func didTapDismissReq(_ sender: UITapGestureRecognizer) {
        hideReqView()
    }
}

extension BrowseViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UserImageCollectionViewCell.identifier, for: indexPath) as? UserImageCollectionViewCell else {return UICollectionViewCell()}
        let user = userImages[indexPath.item]
        if let url = URL(string: user) {
            cell.imgUser.sd_setImage(with: url)
        } else {
            cell.imgUser.image = UIImage(named: "profile-pic")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleIndex = Int(collectionUserimages.contentOffset.x / collectionUserimages.frame.width)
        pageControl.currentPage = visibleIndex
    }
    
    func requestedItemsSheet() {
        if !requestImgViews.isEmpty {
            requestImgViews.removeAll()
            sendReqStkView.arrangedSubviews.forEach({$0.removeFromSuperview()})
        }
        requestedData.enumerated().forEach { (index, item) in
            
            let lbl = UILabel()
            lbl.text = item.itemHeading
            lbl.font = UIFont.lato(ofSize: 16, weight: .bold)
            
            let lblDesc = UILabel()
            lblDesc.text = item.itemDescription
            lblDesc.font = UIFont.lato(ofSize: 12, weight: .regular)
            
            let stk = UIStackView(arrangedSubviews: [lbl, lblDesc])
            stk.alignment = .leading
            stk.axis = .vertical
            stk.spacing = 6
            
            let imgCheck = UIImageView(image: UIImage(named: "ic_uncheck"))
            imgCheck.translatesAutoresizingMaskIntoConstraints = false
            imgCheck.widthAnchor.constraint(equalToConstant: 50).isActive = true
            imgCheck.tintColor = ThemeColor.darkGreen
            imgCheck.tag = index
            imgCheck.contentMode = .scaleAspectFit
            
            let stkHorizontal = UIStackView(arrangedSubviews: [imgCheck, stk])
            let tap = UITapGestureRecognizer(target: self, action: #selector(didTapSelectReq(_:)))
            tap.name = "\(index)"
            stkHorizontal.addGestureRecognizer(tap)
            stkHorizontal.alignment = .center
            stkHorizontal.axis = .horizontal
            stkHorizontal.distribution = .fillProportionally
            stkHorizontal.spacing = 16
            
            requestImgViews.append(imgCheck)
            sendReqStkView.addArrangedSubview(stkHorizontal)
        }
    }
    
    func validated() -> Bool {
        if txtAmount.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Email", message: "Please Enter Valid Amount")
            return false
        } else if txtMessage.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Password", message: "Please Enter Valid Message")
            return false
        } else {
            return true
        }
    }
}
