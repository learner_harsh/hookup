//
//  NotificationsViewController.swift
//  Hookup
//
//  Created by Mayur Boghani on 05/04/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class NotificationsVC: UIViewController {
    
    @IBOutlet weak var tblNotifications: UITableView! {
        didSet {
            tblNotifications.register(UINib(nibName: NotificationCell.identifier, bundle: nil), forCellReuseIdentifier: NotificationCell.identifier)
            tblNotifications.delegate = self
            tblNotifications.dataSource = self
            tblNotifications.tableFooterView = UIView()
        }
    }
    
    @IBOutlet weak var segmentControl: UISegmentedControl! {
        didSet {
            segmentControl.addTarget(self, action: #selector(didTapTab(_:)), for: .valueChanged)
        }
    }
    
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
    }
    
    var totalData: Dataa?
    var notificationArr = [RequestsSent]() {
        didSet {
            DispatchQueue.main.async {
                self.tblNotifications.reloadData()
            }
        }
    }
    
    var isSent: Bool! {
        didSet {
            notificationArr = isSent ? totalData?.requestsSent ?? [] : totalData?.requestReceived ?? []
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.isSent = true
        getMyRequests()
    }
    
    @objc func didTapTab(_ sender: UISegmentedControl) {
        isSent = sender.selectedSegmentIndex == 0
    }
    
    @objc func didTapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMyRequests() {
        NetworkServices.manager.getMyRequests(onSuccess: { (requestedData) in
            print("Send Requested Data == ", requestedData)
            self.totalData = requestedData
            self.notificationArr = requestedData?.requestsSent ?? []
        }, onCompletion: {
            
        })
    }

}

extension NotificationsVC: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        isSent = item.tag == 0
        self.tblNotifications.reloadData()
    }
}

extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.identifier, for: indexPath) as? NotificationCell else {return UITableViewCell()}
        let dict = notificationArr[indexPath.row]
        cell.lblDesc.text = dict.message
        cell.delegate = self
        cell.request = dict
        cell.requestStatusCallback = { [weak self] isAccepted in
            guard let self = `self` else {return}
            self.acceptRejectReq(isAccept: isAccepted, reqID: dict.requestId ?? "")
        }
        if isSent {
            cell.viewActions.isHidden = false
            cell.btnAccept.isEnabled = true
            cell.btnAccept.isHidden = true
            cell.btnReject.isHidden = true
            cell.btnPending.isHidden = dict.status != "1"
//            cell.btnFeedback.isHidden = dict.status == "1"
            cell.btnAccept.setTitle(Constant.accepted.localized, for: .normal)
            cell.btnReject.setTitle(Constant.reject.localized, for: .normal)
            cell.btnPending.setTitle(Constant.pending.localized, for: .normal)
        } else {
//            2 = Accept, 3 = Reject
            cell.viewActions.isHidden = false
            if dict.status == "2" || dict.status == "3" {
                cell.btnAccept.isEnabled = false
                cell.btnReject.isEnabled = false
                cell.btnPending.isEnabled = true

//                cell.btnFeedback.isHidden = false
                cell.btnAccept.isHidden = dict.status == "3"
                cell.btnReject.isHidden = dict.status == "2"
                cell.btnPending.isHidden = true
                cell.btnAccept.setTitle(Constant.accepted.localized, for: .normal)
                cell.btnReject.setTitle(Constant.rejected.localized, for: .normal)
            } else {
                cell.btnAccept.isEnabled = true
                cell.btnReject.isEnabled = true
                cell.btnPending.isEnabled = true
                cell.btnAccept.isHidden = false
                cell.btnReject.isHidden = false
                cell.btnPending.isHidden = true
//                cell.btnFeedback.isHidden = false
                cell.btnAccept.setTitle(Constant.accept.localized, for: .normal)
                cell.btnReject.setTitle(Constant.reject.localized, for: .normal)
            }
            if let feedbackOption = dict.feedbackOption, feedbackOption == true {
                cell.btnFeedback.isHidden = false
            } else {
                cell.btnFeedback.isHidden = true
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = notificationArr[indexPath.row]
        showFeedback(request: dict)
    }
    
    func acceptRejectReq(isAccept: Bool, reqID: String) {
        let param = ["request_id": reqID,
                     "status_to_update": isAccept ? "2" : "3"]
        NetworkServices.manager.acceptRejectReq(params: param, onSuccess: { (requestedData) in
    
            showAlertWithDismiss(title: "Success!", message: requestedData ?? "") { (_) in
                self.getMyRequests()
            }
        }, onCompletion: {
            
        })
    }
}

extension NotificationsVC: NotificationCellDelegate {
    func showFeedback(request: RequestsSent) {
        if request.status != "1" {
            self.navigationController?.pushViewController(FeedbackVC.create(requestId: request.requestId ?? "0"), animated: true)
        }
        
    }
}
