//
//  RootViewController.swift
//  Hookup
//
//  Created by Mayur Boghani on 10/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnSignup: UIButton! {
        didSet {
            btnSignup.addTarget(self, action: #selector(didTapSignUp(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnSignin: UIButton! {
        didSet {
            btnSignin.addTarget(self, action: #selector(didTapSignin(_:)), for: .touchUpInside)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        // Do any additional setup after loading the view.
    }
    
    func setupLocalization() {
        lblWelcome.attributedText = Constant.welcome.localized.fontWithSpacing
        lblDescription.text = Constant.flirtchat.localized
        btnSignup.setTitle(Constant.signUpnow.localized.uppercased(), for: .normal)
        btnSignin.setTitle(Constant.signIn.localized.uppercased(), for: .normal)
        lblWelcome.textColor = ThemeColor.textColor
        lblDescription.textColor = ThemeColor.textColor
        setupFont()
    }
    
    func setupFont() {
        lblWelcome.font = UIFont.lato(ofSize: 34, weight: .regular)
        lblDescription.font = UIFont.lato(ofSize: 16, weight: .light)
        btnSignup.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        btnSignin.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
    }
    
    @objc func didTapSignin(_ sender: UIButton) {
        guard let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController else {return}
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @objc func didTapSignUp(_ sender: UIButton) {
        guard let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {return}
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }

}
