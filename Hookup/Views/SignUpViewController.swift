//
//  SignUpViewController.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnSignup: UIButton! {
        didSet {
            btnSignup.addTarget(self, action: #selector(didTapSignUp(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var lblbTitle: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
//    @IBOutlet weak var viewBack: UIView! {
//        didSet {
//            let tap = UITapGestureRecognizer(target: self, action: #selector(didTapDismiss(_:)))
//            viewBack.isUserInteractionEnabled = true
//            viewBack.addGestureRecognizer(tap)
//        }
//    }
    let storagemngr = StorageManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
        
    }
    
    fileprivate func setupLayout() {
        lblbTitle.text = Constant.signup.localized
        lblUsername.text = Constant.username.localized.uppercased()
        lblEmail.text = Constant.email.localized.uppercased()
        lblPhone.text = Constant.phone.localized.uppercased()
        lblPassword.text = Constant.password.localized.uppercased()
        //        txtUsername.placeholder = "".localized
        //        txtEmail.placeholder = "".localized
        //        txtPhone.placeholder = "".localized
        //        txtPassword.placeholder = "".localized
        
        lblbTitle.font = UIFont.lato(ofSize: 36, weight: .light)
        lblUsername.font = UIFont.lato(ofSize: 16, weight: .light)
        lblEmail.font = UIFont.lato(ofSize: 16, weight: .light)
        lblPhone.font = UIFont.lato(ofSize: 16, weight: .light)
        lblPassword.font = UIFont.lato(ofSize: 16, weight: .light)
        txtUsername.font = UIFont.lato(ofSize: 14, weight: .light)
        txtEmail.font = UIFont.lato(ofSize: 14, weight: .light)
        txtPhone.font = UIFont.lato(ofSize: 14, weight: .light)
        txtPassword.font = UIFont.lato(ofSize: 14, weight: .light)
        
        btnSignup.setTitle(Constant.signup.localized.uppercased(), for: .normal)
        btnSignup.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        
        
        lblbTitle.textColor = ThemeColor.textColor
        lblUsername.textColor = ThemeColor.textColor
        lblEmail.textColor = ThemeColor.textColor
        lblPhone.textColor = ThemeColor.textColor
        lblPassword.textColor = ThemeColor.textColor
        txtUsername.textColor = ThemeColor.textColor
        txtEmail.textColor = ThemeColor.textColor
        txtPhone.textColor = ThemeColor.textColor
        txtPassword.textColor = ThemeColor.textColor
    }
    
    @objc func didTapSignUp(_ sender: UIButton) {
        if validated() {
            let param = [
                "first_name": txtUsername.text!,
                "last_name": "test",
                "email": txtEmail.text!,
                "password": txtPassword.text!,
                "confirm_password": txtPassword.text!,
                "contact_number": txtPhone.text!
            ] as [String : Any]
            
            NetworkServices.manager.signUp(params: param) { (users) in
                print(users)
                if ((self.navigationController?.viewControllers.contains(where: {$0.isKind(of: SignInViewController.self)})) != nil) {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    guard let signUpBVC = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController else {return}
                    self.navigationController?.pushViewController(signUpBVC, animated: true)
                }
            } onCompletion: {
                
            }
        }
    }
    
    
    @objc func didTapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapDismiss(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validated() -> Bool {
        if !(txtEmail.text?.isValidEmail ?? false || txtEmail.text?.trimmingCharacters(in: .whitespaces) == "") {
            showAlert(title: "Invalid Email", message: "Please Enter Valid Email")
            return false
        } else if txtPassword.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Password", message: "Please Enter Valid Password")
            return false
        } else if txtPhone.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Phone Number", message: "Please Enter Valid Phone Number")
            return false
        } else if txtUsername.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Username", message: "Please Enter Valid Username")
            return false
        } else {
            return true
        }
    }
}
