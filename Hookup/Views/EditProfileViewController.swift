//
//  EditProfileViewController.swift
//  Hookup
//
//  Created by Milan Sanghani on 23/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnSave: UIButton! {
        didSet {
            btnSave.addTarget(self, action: #selector(didTapSave(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var segmentGender: UISegmentedControl!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet var btnAdd: [UIButton]! {
        didSet {
            addTargetToBtn()
        }
    }
    @IBOutlet var imgViews: [UIImageView]!
    var isFromLogin = false
    var userid = ""
    var userDetails: ProfileData? {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.setData()
            }
        }
    }
    var currentTappedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFromLogin {
            getDataFromAPI()
        }
    }
    
    fileprivate func setupLayout() {
        self.navigationController?.isNavigationBarHidden = true
        lblName.text = Constant.name.localized
        lblAddress.text = Constant.location.localized
        lblDob.text = Constant.dob.localized
        lblGender.text = Constant.gender.localized
        
        lblName.font = UIFont.lato(ofSize: 16, weight: .light)
        lblAddress.font = UIFont.lato(ofSize: 16, weight: .light)
        txtName.font = UIFont.lato(ofSize: 14, weight: .light)
        txtName.keyboardType = .numberPad
        txtAddress.font = UIFont.lato(ofSize: 14, weight: .light)
        lblDob.font = UIFont.lato(ofSize: 16, weight: .light)
        lblGender.font = UIFont.lato(ofSize: 16, weight: .light)
        txtDob.font = UIFont.lato(ofSize: 14, weight: .light)
        
        btnSave.setTitle(Constant.save.localized.uppercased(), for: .normal)
        btnSave.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        
        lblName.textColor = ThemeColor.textColor
        lblAddress.textColor = ThemeColor.textColor
        txtName.textColor = ThemeColor.textColor
        txtAddress.textColor = ThemeColor.textColor
        lblDob.textColor = ThemeColor.textColor
        lblGender.textColor = ThemeColor.textColor
        txtDob.textColor = ThemeColor.textColor
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        txtDob.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        if #available(iOS 14, *) {
            datePickerView.preferredDatePickerStyle = .wheels
        }
        
        txtName.placeholder = "Enter Postcode"
        txtAddress.placeholder = "Enter Location"
        txtDob.placeholder = "Enter Birthdate"
        
        btnBack.isHidden = isFromLogin
    }
    
    private func setData() {
        if let user = userDetails {
            txtName.text = user.postcode
            txtAddress.text = user.location
            txtDob.text = user.dob
            segmentGender.selectedSegmentIndex = Int(user.gender ?? "1") ?? 1
            let profilee = user.profileImgs?.first(where: {$0.isProfilePic == "1"})
            let otherImg = user.profileImgs?.filter({$0.isProfilePic == "0"})
            imgViews.forEach { (img) in
                if img.tag == 0 {
                    let imgurl = URL(string: profilee?.imageName ?? "")!
                    img.sd_setImage(with: imgurl)
                } else {
                    guard let image =  otherImg?[safe: img.tag-1]?.imageName, !image.isEmpty else { return }
                    let imgurl = URL(string: image)!
                    img.sd_setImage(with: imgurl)
                }
            }
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtDob.text = dateFormatter.string(from: sender.date)
    }
    
    private func addTargetToBtn() {
        btnAdd.forEach { (btn) in
            btn.addTarget(self, action: #selector(didTapAddImage(_:)), for: .touchUpInside)
        }
    }
    
    @objc func didTapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapSave(_ sender: UIButton) {
        var params = [String: Any]()
        for img in imgViews {
            if let imgData = img.image?.jpegData(compressionQuality: 0.7) {
                if img.tag == 0 {
                    params["profile_img"] = imgData
                } else if img.tag == 1 {
                    params["other_img_1"] = imgData
                } else {
                    params["other_img_2"] = imgData
                }
            }
        }
        params["gender"] = segmentGender.selectedSegmentIndex
        params["dob"] = txtDob.text ?? ""
        params["postcode"] = txtName.text ?? ""
        params["location"] = txtAddress.text ?? ""
        
        NetworkServices.manager.saveProfileData(params: params) { (messge) in
            print(messge)
            showAlertWithDismiss(title: "Success!", message: messge ?? "") {_ in
                DispatchQueue.main.async {
                    if self.isFromLogin {
                        self.gotoServices()
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
        } onCompletion: {
            
        }
    }
    
    func gotoServices() {
        guard let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "ServicesSettingsViewController") as? ServicesSettingsViewController else {return}
        self.navigationController?.pushViewController(settingVC, animated: true)
    }
    
    @objc func didTapAddImage(_ sender: UIButton) {
        currentTappedIndex = sender.tag
        present(from: sender)
    }
    
    func getDataFromAPI() {
        NetworkServices.manager.getProfileData(userId: userid, onSuccess: { (profiledata) in
            print("Get Profile Data == ", profiledata)
            guard let dataa = profiledata else {return}
            self.userDetails = dataa
        }, onCompletion: {
            
        })
        
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            let pickerController = UIImagePickerController()
            pickerController.delegate = self
            pickerController.allowsEditing = true
            pickerController.mediaTypes = ["public.image"]
            DispatchQueue.main.async {
                self.present(pickerController, animated: true)
            }
        }
    }

    public func present(from sourceView: UIView) {

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        self.present(alertController, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
        self.didSelect(image: image)
    }

    func didSelect(image: UIImage?) {
        for img in imgViews where img.tag == currentTappedIndex {
            img.image = image
        }
    }
}
