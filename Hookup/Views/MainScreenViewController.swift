//
//  MainScreenViewController.swift
//  Hookup
//
//  Created by Mayur Boghani on 11/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessageCount: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var collectionUsers: UICollectionView! {
        didSet {
            collectionUsers.delegate = self
            collectionUsers.dataSource = self
            collectionUsers.register(UINib(nibName: "UsersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UsersCollectionViewCell")
        }
    }
    var userList = [ProfileData]() {
        didSet {
            DispatchQueue.main.async {
                self.collectionUsers.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        configureSideMenu()
//        getItems()
//        userList = [Users(profile: "profile-thumb", images: ["profile-thumb", "profile-pic"], email: "mayur@gmail.com", name: "Aleena", age: "24", status: "1", address: "Sydney, Australia"),
//                    Users(profile: "profile-thumb", images: ["profile-thumb", "profile-pic"], email: "mayurc@gmail.com", name: "Aleena", age: "24", status: "0", address: "Sydney, Australia")]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDataFromAPI()
    }
    
    func getDataFromAPI() {
        NetworkServices.manager.getUsers(onSuccess: { (userslistData) in
            print("Get Profile Data == ", userslistData)
            guard let dataa = userslistData else {return}
            self.userList = dataa
        }, onCompletion: {
            
        })

    }
    
    private func setupLayout() {
        self.navigationController?.isNavigationBarHidden = true
        StorageManager.shared.currentScreen = .home
        lblTitle.text = Constant.profilelisting.localized
        lblTitle.font = UIFont.lato(ofSize: 18, weight: .bold)
        lblMessageCount.layer.cornerRadius = lblMessageCount.bounds.height / 2
        lblMessageCount.clipsToBounds = true
        btnMenu.addTarget(self, action: #selector(didTapMenu(_:)), for: .touchUpInside)
        btnChat.addTarget(self, action: #selector(didTapChat(_:)), for: .touchUpInside)
    }
    
    private func configureSideMenu() {
        SideMenuController.preferences.basic.menuWidth = 240
        SideMenuController.preferences.basic.defaultCacheKey = "0"
    }
    
    
    @objc func didTapMenu(_ sender: UIButton) {
        sideMenuController?.revealMenu()
    }
    
    
    @objc func didTapChat(_ sender: UIButton) {
        guard let notificatonVC = storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC else {return}
        self.navigationController?.pushViewController(notificatonVC, animated: true)
    }
    

}

extension MainScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as? UsersCollectionViewCell else {return UICollectionViewCell()}
        let user = userList[indexPath.item]
        cell.configCell(user)
//        if let url = URL(string: user.profileImg ?? "") {
//            cell.imgUser.sd_setImage(with: url, completed: { (image, error, cache, urll) in
//                print(image)
//            })
//        } else {
//            cell.imgUser.image = UIImage()
//        }
//        cell.lblLocation.text = user.location ?? "London"
//        cell.lblNameAge.text = "\(user.firstName ?? "") \(user.lastName ?? ""), \(user.dob?.age ?? "")"
//        cell.status = .offline//user.status == "0" ? .offline : .online
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let browseVC = storyboard?.instantiateViewController(withIdentifier: "BrowseViewController") as? BrowseViewController else {return}
        browseVC.userid = userList[indexPath.item].id ?? ""
        self.navigationController?.pushViewController(browseVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width * 0.49, height: collectionView.bounds.width * 0.55)
    }
}
