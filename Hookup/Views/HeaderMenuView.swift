//
//  HeaderMenuView.swift
//  Hookup
//
//  Created by Mayur Boghani on 12/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit
import SideMenuSwift

class HeaderMenuView: UIView {
    
    static let identifier = "HeaderMenuView"
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessageCount: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    private func setupLayout() {
        lblTitle.font = UIFont.lato(ofSize: 18, weight: .bold)
        lblMessageCount.layer.cornerRadius = lblMessageCount.bounds.height / 2
        lblMessageCount.clipsToBounds = true
        btnMenu.addTarget(self, action: #selector(didTapMenu(_:)), for: .touchUpInside)
        btnChat.addTarget(self, action: #selector(didTapChat(_:)), for: .touchUpInside)
    }
    
    @objc func didTapMenu(_ sender: UIButton) {
//        sideMenuController?.revealMenu()
    }
    
    
    @objc func didTapChat(_ sender: UIButton) {
        
    }
    
}
