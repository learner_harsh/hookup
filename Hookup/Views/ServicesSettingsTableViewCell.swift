//
//  ServicesSettingsTableViewCell.swift
//  Hookup
//
//  Created by Mayur Boghani on 12/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class ServicesSettingsTableViewCell: UITableViewCell {
    
    static let identifier = "ServicesSettingsTableViewCell"
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    var didTapCheck: (() -> Void)?
    var didTapTitle: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        btnTitle.setTitleColor(ThemeColor.textColor, for: .normal)
        btnTitle.titleLabel?.font = UIFont.lato(ofSize: 18, weight: .bold)
        lblDescription.font = UIFont.lato(ofSize: 14, weight: .light)
        
        btnCheck.addTarget(self, action: #selector(didTapCheck(_:)), for: .touchUpInside)
        btnTitle.addTarget(self, action: #selector(didTapTitle(_:)), for: .touchUpInside)
    }
    
    @objc func didTapCheck(_ sender: UIButton) {
        didTapCheck?()
    }
    
    @objc func didTapTitle(_ sender: UIButton) {
        didTapTitle?()
    }
    
    
}
