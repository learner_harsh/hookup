//
//  ServicesSettingsViewController.swift
//  Hookup
//
//  Created by Mayur Boghani on 12/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class ServicesSettingsViewController: UIViewController {
    
    @IBOutlet weak var tblSettings: UITableView! {
        didSet {
            tblSettings.delegate = self
            tblSettings.dataSource = self
            tblSettings.tableFooterView = UIView()
            tblSettings.register(UINib(nibName: ServicesSettingsTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ServicesSettingsTableViewCell.identifier)
        }
    }
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnSendRequest: UIButton!
    var servicesList = [RequestItems]() {
        didSet {
            DispatchQueue.main.async {
                self.tblSettings.reloadData()
            }
        }
    }
    var tappedIndex = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        btnSendRequest.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        btnSendRequest.setTitle(Constant.save.localized, for: .normal)
        btnSendRequest.addTarget(self, action: #selector(didTapSendRequest(_:)), for: .touchUpInside)
        StorageManager.shared.currentScreen = .services
        getItems()
//        if let headerview = Bundle.main.loadNibNamed(HeaderMenuView.identifier, owner: nil, options: nil)?.first as? HeaderMenuView {
//            headerview.lblTitle.text = Constant.browse.localized
//            headerview.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeader.bounds.height)
//            viewHeader.addSubview(headerview)
//        }
    }
    
    func getItems() {
        NetworkServices.manager.getRequestItems { (req) in
            print("GetItems == ", req)
            guard let reqq = req else {return}
            self.servicesList = reqq
        } onCompletion: {

        }
    }

    func saveItems() {
        let checkedServices = servicesList.filter({$0.checked == true}).compactMap({$0.id}).joined(separator: ",")
        NetworkServices.manager.saveRequestItems(itemids: checkedServices, onSuccess: { (reqMessage) in
            print("SaveItems == ", reqMessage)
            DispatchQueue.main.async {
                showAlertWithDismiss(title: "Success!", message: reqMessage ?? "") { (action) in
                    StorageManager.shared.currentScreen = .home
                    UIApplication.getRootView()
                }
            }
        }, onCompletion: {

        })
    }
    
    @objc func didTapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapSendRequest(_ sender: UIButton) {
        saveItems()
    }

}

extension ServicesSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ServicesSettingsTableViewCell.identifier, for: indexPath) as? ServicesSettingsTableViewCell else {return UITableViewCell()}
        let dict = servicesList[indexPath.row]
        cell.btnTitle.setTitle(dict.itemHeading, for: .normal)
        cell.lblDescription.text = dict.itemDescription
        let ischecked = dict.checked ?? false
        if ischecked {
            cell.btnCheck.setImage(UIImage(named: "ic_check"), for: .normal)
            cell.btnCheck.tintColor = .orange
        } else {
            cell.btnCheck.setImage(UIImage(named: "ic_uncheck"), for: .normal)
            cell.btnCheck.tintColor = .lightGray
        }
        cell.lblDescription.isHidden = !(indexPath.row == tappedIndex)
        cell.imgArrow.transform = indexPath.row == tappedIndex ? CGAffineTransform(rotationAngle: .pi) : .identity
        cell.didTapCheck = { [weak self] in
            guard let self = `self` else {return}
            var ischecked = dict.checked ?? false
            ischecked.toggle()
            self.servicesList[indexPath.row].checked = ischecked
            self.tblSettings.reloadData()
        }
        cell.didTapTitle = { [weak self] in
            guard let self = `self` else {return}
            self.tappedIndex = indexPath.row
            self.tblSettings.reloadData()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 50))
        let lbl = UILabel(frame: CGRect(x: 16, y: 0, width: tableView.bounds.width, height: 50))
        vw.backgroundColor = .groupTableViewBackground
        lbl.backgroundColor = .clear
        lbl.text = "GENERAL"
        lbl.textAlignment = .left
        lbl.font = UIFont.lato(ofSize: 16, weight: .light)
        vw.addSubview(lbl)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

