//
//  UserImageCollectionViewCell.swift
//  Hookup
//
//  Created by Mayur Boghani on 12/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class UserImageCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "UserImageCollectionViewCell"

    @IBOutlet weak var imgUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgUser.layer.cornerRadius = 12
        imgUser.clipsToBounds = true
    }

}
