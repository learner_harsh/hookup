//
//  NotificationCell.swift
//  Hookup
//
//  Created by Mayur Boghani on 05/04/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

protocol NotificationCellDelegate: AnyObject {
    func showFeedback(request: RequestsSent)
}

class NotificationCell: UITableViewCell {
    
    static let identifier = "NotificationCell"
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var btnFeedback: UIButton!
    @IBOutlet weak var viewActions: UIView!
    
    var requestStatusCallback: ((Bool) -> Void)?
    weak var delegate: NotificationCellDelegate?
    var request: RequestsSent!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    func setupViews() {
        lblDesc.font = UIFont.lato(ofSize: 18, weight: .bold)
        lblDesc.textColor = ThemeColor.textColor
        btnAccept.titleLabel?.font = UIFont.lato(ofSize: 12, weight: .bold)
        btnAccept.setTitle(Constant.accept.localized, for: .normal)
        btnAccept.addTarget(self, action: #selector(didTapAccept(_:)), for: .touchUpInside)
        btnAccept.backgroundColor = UIColor.green.withAlphaComponent(0.2)
        btnAccept.setTitleColor(.green, for: .normal)
        btnAccept.layer.cornerRadius = btnAccept.bounds.height / 2
        btnAccept.clipsToBounds = true
        
        btnReject.titleLabel?.font = UIFont.lato(ofSize: 12, weight: .bold)
        btnReject.setTitle(Constant.reject.localized, for: .normal)
        btnReject.addTarget(self, action: #selector(didTapReject(_:)), for: .touchUpInside)
        btnReject.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        btnReject.setTitleColor(.red, for: .normal)
        btnReject.layer.cornerRadius = btnReject.bounds.height / 2
        btnReject.clipsToBounds = true
        
        btnPending.titleLabel?.font = UIFont.lato(ofSize: 12, weight: .bold)
        btnPending.setTitle(Constant.pending.localized, for: .normal)
        btnPending.addTarget(self, action: #selector(didTapPending(_:)), for: .touchUpInside)
        btnPending.backgroundColor = UIColor.orange.withAlphaComponent(0.2)
        btnPending.setTitleColor(.orange, for: .normal)
        btnPending.layer.cornerRadius = btnPending.bounds.height / 2
        btnPending.clipsToBounds = true
        
        
        btnFeedback.titleLabel?.font = UIFont.lato(ofSize: 12, weight: .bold)
        btnFeedback.setTitle(Constant.feedback.localized, for: .normal)
        btnFeedback.addTarget(self, action: #selector(didTapFeedback(_:)), for: .touchUpInside)
        btnFeedback.backgroundColor = UIColor.purple.withAlphaComponent(0.2)
        btnFeedback.setTitleColor(.purple, for: .normal)
        btnFeedback.layer.cornerRadius = btnFeedback.bounds.height / 2
        btnFeedback.clipsToBounds = true
        
    }
    
    @objc func didTapFeedback(_ sender: UIButton) {
        self.delegate?.showFeedback(request: request)
    }
    
    @objc func didTapPending(_ sender: UIButton) {
        
    }
    
    @objc func didTapAccept(_ sender: UIButton) {
        requestStatusCallback?(true)
    }
    
    @objc func didTapReject(_ sender: UIButton) {
        requestStatusCallback?(false)
    }
    
}
