//
//  UsersCollectionViewCell.swift
//  Hookup
//
//  Created by Mayur Boghani on 11/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

enum ProfileStatus: String {
    case online = "online"
    case offline = "offline"
}

class UsersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblNameAge: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblRating: UILabel!
    
    var status = ProfileStatus.online {
        didSet {
            viewStatus.backgroundColor = status == .online ? .green : .red
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imgUser.layer.cornerRadius = imgUser.bounds.height / 2
        imgUser.clipsToBounds = true
    }
    
    private func setupLayout() {
        viewMain.layer.cornerRadius = 12
        viewMain.addShadow()
        lblNameAge.font = UIFont.lato(ofSize: 18, weight: .bold)
        lblLocation.font = UIFont.lato(ofSize: 14, weight: .light)
        viewStatus.layer.cornerRadius = viewStatus.bounds.height / 2
        viewStatus.clipsToBounds = true
    }
    
    func configCell(_ user: ProfileData) {
        if let url = URL(string: user.profileImg ?? "") {
            imgUser.sd_setImage(with: url, completed: { (image, error, cache, urll) in
                print(image)
            })
        } else {
            imgUser.image = UIImage()
        }
        lblLocation.text = user.location ?? ""
        lblNameAge.text = "\(user.firstName ?? "") \(user.lastName ?? ""), \(user.dob?.age ?? "")"
        status = .offline//user.status == "0" ? .offline : .online
        lblRating.text = "\(user.ratings ?? 0)"
    }
    
}
