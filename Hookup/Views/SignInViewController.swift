//
//  SignInViewController.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnSignin: UIButton! {
        didSet {
            btnSignin.addTarget(self, action: #selector(didTapSignIn(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnForgotPassword: UIButton! {
        didSet {
            btnForgotPassword.addTarget(self, action: #selector(didTapForgotPassword(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnSignup: UIButton! {
        didSet {
            btnSignup.addTarget(self, action: #selector(didTapSignUp(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var lblbTitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    let storagemngr = StorageManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    fileprivate func setupLayout() {
        self.navigationController?.isNavigationBarHidden = true
        lblbTitle.text = Constant.signIn.localized
        lblEmail.text = Constant.username.localized.uppercased()
        lblPassword.text = Constant.password.localized.uppercased()
        //        txtEmail.placeholder = "".localized
        //        txtPassword.placeholder = "".localized
        
        lblbTitle.font = UIFont.lato(ofSize: 36, weight: .light)
        lblEmail.font = UIFont.lato(ofSize: 16, weight: .light)
        lblPassword.font = UIFont.lato(ofSize: 16, weight: .light)
        txtEmail.font = UIFont.lato(ofSize: 14, weight: .light)
        txtPassword.font = UIFont.lato(ofSize: 14, weight: .light)
        txtPassword.passwordRules = .none
        
        btnSignin.setTitle(Constant.signIn.localized.uppercased(), for: .normal)
        btnSignin.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .bold)
        btnForgotPassword.setTitle(Constant.forgotPass.localized, for: .normal)
        btnForgotPassword.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .light)
        btnSignup.setTitle(Constant.alreadyaccount.localized, for: .normal)
        btnSignup.titleLabel?.font = UIFont.lato(ofSize: 16, weight: .regular)
        
        
        lblbTitle.textColor = ThemeColor.textColor
        lblEmail.textColor = ThemeColor.textColor
        lblPassword.textColor = ThemeColor.textColor
        txtEmail.textColor = ThemeColor.textColor
        txtPassword.textColor = ThemeColor.textColor
        btnForgotPassword.setTitleColor(ThemeColor.textColor, for: .normal)
    }
    
    @objc func didTapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapSignIn(_ sender: UIButton) {
        if validated() {
            let deviceid = UUID().uuidString
            let param = [
                "email": txtEmail.text!,
                "password": txtPassword.text!,
                "device_id": deviceid,
                "device_type": 1
            ] as [String : Any]
            
            NetworkServices.manager.checkLogin(params: param) { (users) in
                if users?.isFirstLogin == 1 {
                    guard let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController else {return}
                    editProfileVC.isFromLogin = true
                    StorageManager.shared.currentScreen = .profile
                    self.navigationController?.pushViewController(editProfileVC, animated: true)
                } else {
                    StorageManager.shared.currentScreen = .home
                    UIApplication.getRootView()
                }
            } onCompletion: {
                
            }
        }
    }
    
    @objc func didTapSignUp(_ sender: UIButton) {
        guard let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {return}
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @objc func didTapForgotPassword(_ sender: UIButton) {
        

    }
    
    func validated() -> Bool {
        if txtEmail.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Email", message: "Please Enter Valid Username")
            return false
        } else if txtPassword.text?.trimmingCharacters(in: .whitespaces) == "" {
            showAlert(title: "Invalid Password", message: "Please Enter Valid Password")
            return false
        } else {
            return true
        }
    }
}
