//
//  SceneDelegate.swift
//  BUZZ
//
//  Created by Mayur Boghani on 27/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        if let appDel = UIApplication.shared.delegate as? AppDelegate {
            appDel.window = window
        }
        setRootVC()
    }

    func setRootVC() {
        if StorageManager.shared.isUserLogin {
            guard let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as? MainScreenViewController else {return}
            guard let vcMenu = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController else {return}
            let sidemenu = SideMenuController(contentViewController: mainVC, menuViewController: vcMenu)
            sidemenu.navigationController?.isNavigationBarHidden = true
            window?.rootViewController = UINavigationController(rootViewController: sidemenu)
            window?.makeKeyAndVisible()
        } else {
            if StorageManager.shared.currentScreen == .profile {
                guard let editVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController else {return}
                window?.rootViewController = UINavigationController(rootViewController: editVc)
                window?.makeKeyAndVisible()
            } else if StorageManager.shared.currentScreen == .services {
                guard let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ServicesSettingsViewController") as? ServicesSettingsViewController else {return}
                window?.rootViewController = UINavigationController(rootViewController: mainVC)
                window?.makeKeyAndVisible()
            } else {
                guard let signUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController  else {return}
                window?.rootViewController = UINavigationController(rootViewController: signUpVC)
                window?.makeKeyAndVisible()
            }
        }
    }

}

