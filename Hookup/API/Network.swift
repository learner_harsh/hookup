//
//  Network.swift
//  Buzz
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Foundation
import Moya
import Alamofire

private class CustomServerTrustPoliceManager: ServerTrustManager {
    
    override func serverTrustEvaluator(forHost host: String) throws -> ServerTrustEvaluating? {
        return .none
    }
    
    public init() {
        super.init(evaluators: [:])
    }
}

struct Network {
    typealias DecodingData<T> = (type: T.Type, decoder: JSONDecoder)
    private static var lastErrorThrowTime = Date(timeIntervalSince1970: 0)
    
    static var provider: MoyaProvider<HookupAPI> {
        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 12
        let manager = Session(configuration: configuration, serverTrustManager: CustomServerTrustPoliceManager())
        let provider = MoyaProvider<HookupAPI>(session: manager)
        return provider
    }
    
    @discardableResult
    static func requestArr<T>(
        _ target: HookupAPI,
        decoder: JSONDecoder = JSONDecoder(),
        dispatchQueue: DispatchQueue? = nil,
        success successCallback: @escaping (_ data: [T]) -> Void,
        error errorCallback: @escaping (_ errorr: ErrorModel) -> Void,
        failure failureCallback: @escaping (MoyaError) -> Void,
        completion completionCallback: @escaping () -> Void) -> Cancellable where T: Decodable {
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        let cancellableRequest = provider.request(target) { result in
            completionCallback()
            switch result {
            case let .success(response):
                let statusCode = HTTPStatusCode(rawValue: response.statusCode) ?? HTTPStatusCode.ok
                if !statusCode.isSuccess {
                    do {
                        let errorrCall = try JSONDecoder().decode(ErrorModel.self, from: response.data)
                        errorCallback(errorrCall)
                    } catch _ {
                        showAlert(title: "Opps!", message: "Something went Wrong")
                    }
                } else {
                    do {
                        let result = try JSONDecoder().decode([T].self, from: response.data)
                        successCallback(result)
                    } catch _ {
                        showAlert(title: "Opps!", message: "Something went Wrong")
                    }
                }
            case let .failure(error):
                if Date().timeIntervalSince(lastErrorThrowTime) >= 15 {
                    lastErrorThrowTime = Date()
                }
                failureCallback(error)
            }
        }
        return cancellableRequest
    }
    
    @discardableResult
    static func request<T>(
        _ target: HookupAPI,
        decoder: JSONDecoder = JSONDecoder(),
        dispatchQueue: DispatchQueue? = nil,
        success successCallback: @escaping (_ data: T) -> Void,
        error errorCallback: @escaping (_ errorr: ErrorModel) -> Void,
        failure failureCallback: @escaping (MoyaError) -> Void,
        completion completionCallback: @escaping () -> Void) -> Cancellable where T: Decodable {
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        let cancellableRequest = provider.request(target) { result in
            completionCallback()
            switch result {
            case let .success(response):
                let statusCode = HTTPStatusCode(rawValue: response.statusCode) ?? HTTPStatusCode.ok
                if !statusCode.isSuccess {
                    do {
                        let errorrCall = try JSONDecoder().decode(ErrorModel.self, from: response.data)
                        errorCallback(errorrCall)
                    } catch _ {
                        showAlert(title: "Opps!", message: "Something went Wrong")
                    }
                } else {
                    do {
                        let result = try JSONDecoder().decode(T.self, from: response.data)
                        successCallback(result)
                    } catch _ {
                        showAlert(title: "Opps!", message: "Something went Wrong")
                    }
                }
            case let .failure(error):
                if Date().timeIntervalSince(lastErrorThrowTime) >= 15 {
                    lastErrorThrowTime = Date()
                }
                failureCallback(error)
            }
        }
        return cancellableRequest
    }
    
    @discardableResult
    static func requestStatusOnly(
        _ target: HookupAPI,
        decoder: JSONDecoder = JSONDecoder(),
        dispatchQueue: DispatchQueue? = nil,
        success successCallback: @escaping () -> Void,
        error errorCallback: @escaping (_ errorr: ErrorModel) -> Void,
        failure failureCallback: @escaping (MoyaError) -> Void,
        completion completionCallback: @escaping () -> Void) -> Cancellable {
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        let cancellableRequest = provider.request(target) { result in
            completionCallback()
            switch result {
            case let .success(response):
                let statusCode = HTTPStatusCode(rawValue: response.statusCode) ?? HTTPStatusCode.ok
                if statusCode == .conflict {
                    errorCallback(ErrorModel(response: [], status: "409"))
                } else {
                    successCallback()
                }
            case let .failure(error):
                if Date().timeIntervalSince(lastErrorThrowTime) >= 15 {
                    lastErrorThrowTime = Date()
                }
                failureCallback(error)
            }
        }
        return cancellableRequest
    }
}
