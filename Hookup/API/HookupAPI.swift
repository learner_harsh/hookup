//
//  HookupAPI.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Moya

struct BaseURL {
    static let productionUrl = "http://13.233.96.217/projects/dating_app/"
}

enum HookupAPI {
    case checkLogin(params: [String: Any])
    case register(params: [String: Any])
    case getRequestItems
    case getUsers
    case saveRequestItems(itemIds: String)
    case getProfileInfo(userId: String)
    case saveProfileInfo(params: [String: Any])
    case getUserRequestItems(userId: String)
    case sendRequestToUser(params: [String: Any])
    case getMyRequests
    case acceptRejectReq(params: [String: Any])
    case getFeedback(params: [String: Any])
    case saveFeedback(params: [String: Any])
}

extension HookupAPI: TargetType {
    
    var headers: [String: String]? {
        switch self {
        default:
            return nil// ["Content-Type": "application/x-www-form-urlencoded"]
        }
    }
    
    var baseURL: URL {
        switch self {
        default:
            return URL(string: BaseURL.productionUrl)!
        }
    }
    
    var path: String {
        switch self {
        case .checkLogin:
            return "login"
        case .register:
            return "registration"
        case .getUsers:
            return "get_users"
        case .getRequestItems:
            return "get_request_items"
        case .saveRequestItems:
            return "save_request_items_for_user"
        case .getProfileInfo:
            return "get_profile_info"
        case .saveProfileInfo:
            return "save_profile_info"
        case .getUserRequestItems:
            return "get_user_request_items"
        case .sendRequestToUser:
            return "send_request"
        case .getMyRequests:
            return "my_requests"
        case .acceptRejectReq:
            return "accept_reject_requests"
        case .getFeedback:
            return "get_feedbacks"
        case .saveFeedback:
            return "save_feedback"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .checkLogin(let params), .register(let params):
            return params
        case .getRequestItems:
            return ["auth_token": StorageManager.shared.authToken]
        case .saveRequestItems(let itemids):
            return ["auth_token": StorageManager.shared.authToken, "item_ids": itemids]
        case .getProfileInfo(let id), .getUserRequestItems(let id):
            return ["auth_token": StorageManager.shared.authToken, "user_id": id]
        default:
            return nil
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        //        case .checkLogin:
        //            return URLEncoding.queryString
        default:
            return URLEncoding.queryString
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        print(StorageManager.shared.authToken)
        var formData: [Moya.MultipartFormData] = []
        //        formData.append(Moya.MultipartFormData(provider: .data("$2y$10$T/DC7EuvOPR05vAGRXZZVe.BAIPMZbkC2QCH9hoV/MCJwFhKZGRkG".utf8Encoded), name: "auth_token"))
        formData.append(Moya.MultipartFormData(provider: .data(StorageManager.shared.authToken.utf8Encoded), name: "auth_token"))
        switch self {
        case .checkLogin(let params), .register(let params), .saveProfileInfo(let params):
            var formDataa: [Moya.MultipartFormData] = []
            for param in params.keys {
                if let value = params[param] as? Int {
                    let strval = "\(value)"
                    formDataa.append(Moya.MultipartFormData(provider: .data(strval.utf8Encoded), name: param))
                } else if let value = params[param] as? String {
                    formDataa.append(Moya.MultipartFormData(provider: .data(value.utf8Encoded), name: param))
                } else if let value = params[param] as? Data {
                    formDataa.append(Moya.MultipartFormData(provider: .data(value), name: param, fileName: "\(param).jpeg", mimeType: "jpeg"))
                }
            }
            return .uploadMultipart(formDataa)
        case .sendRequestToUser(let params),
             .acceptRejectReq(let params),
             .getFeedback(let params),
             .saveFeedback(let params):
            for param in params.keys {
                if let value = params[param] as? Int {
                    let strval = "\(value)"
                    formData.append(Moya.MultipartFormData(provider: .data(strval.utf8Encoded), name: param))
                } else if let value = params[param] as? String {
                    formData.append(Moya.MultipartFormData(provider: .data(value.utf8Encoded), name: param))
                } else if let value = params[param] as? Data {
                    formData.append(Moya.MultipartFormData(provider: .data(value), name: param, fileName: "\(param).jpeg", mimeType: "jpeg"))
                }
            }
            return .uploadMultipart(formData)
        case .getProfileInfo(let id), .getUserRequestItems(let id):
            formData.append(Moya.MultipartFormData(provider: .data(id.utf8Encoded), name: "user_id"))
            return .uploadMultipart(formData)
        case .getUsers, .getRequestItems, .getMyRequests:
            return .uploadMultipart(formData)
        case .saveRequestItems(let itemids):
            formData.append(Moya.MultipartFormData(provider: .data(itemids.utf8Encoded), name: "item_ids"))
            return .uploadMultipart(formData)
        default:
            if parameters != nil {
                return .requestParameters(parameters: parameters!, encoding: parameterEncoding)
            } else {
                return .requestPlain
            }
        }
    }
}

extension String {
    var utf8Encoded: Data { return data(using: .utf8)! }
}
