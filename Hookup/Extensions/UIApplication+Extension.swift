//
//  UIApplication+Extension.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    class func getRootView() {
        if #available(iOS 13.0, *) {
            if let del = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                del.setRootVC()
            }
        } else {
            if let del = UIApplication.shared.delegate as? AppDelegate {
                del.setRootView()
            }
        }
    }
}
