//
//  Date+Extension.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Foundation

// MARK: - Format month and year from date
extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
    
    var year: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        return dateFormatter.string(from: self)
    }
    
    var ISOStr: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.string(from: self)
    }
    
    var ISOLOCALStr: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: self)
    }
    
    var ISOUTCCStr: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.string(from: self)
    }
    
    var toLocalTime: Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func toCreateISODate(time: String) -> Date {
        let datformt = DateFormatter()
        datformt.timeZone = TimeZone.current
        datformt.dateFormat = "MM/dd/yyyy"
        let dtStr = datformt.string(from: self)
        let startDtstr = "\(dtStr) \(time)"
        datformt.dateFormat = "MM/dd/yyyy H:m"
        let startDt = datformt.date(from: startDtstr) ?? Date()
        return startDt
    }
    
    var toDisplayDate: String {
        let datformt = DateFormatter()
        datformt.timeZone = TimeZone.current
        datformt.dateFormat = "MMM d, h:mm a"
        return datformt.string(from: self)
    }
    
    var toMMddyyyy: String {
        let datformt = DateFormatter()
        datformt.timeZone = TimeZone.current
        datformt.dateFormat = "MM/dd/yyyy"
        let dtStr = datformt.string(from: self)
//        let dt = datformt.date(from: dtStr) ?? Date()
        return dtStr
    }
    
    var toMMddyyyyLOCAL: String {
        let datformt = DateFormatter()
        datformt.dateFormat = "MM/dd/yyyy"
        datformt.timeZone = TimeZone.current
        let dtStr = datformt.string(from: self)
        let dt = datformt.date(from: dtStr) ?? Date()
        datformt.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return datformt.string(from: dt)
    }
    
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate

        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}
