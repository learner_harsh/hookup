//
//  String+Extension.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

extension String {
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    var ISOStr: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let datee = dateFormatter.date(from: self) ?? Date()
        let datformt = DateFormatter()
        datformt.timeZone = TimeZone.current
        datformt.dateFormat = "MMM d, h:mm a"
        return datformt.string(from: datee)
    }
    
    var toTime: String {
        let datformt = DateFormatter()
        datformt.timeZone = TimeZone.current
        datformt.dateFormat = "H:m"
        let dt = datformt.date(from: self) ?? Date()
        datformt.dateFormat = "hh:mm a"
        return datformt.string(from: dt)
    }
    
    var toMMddyyyy: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dtStr = dateFormatter.string(from: date)
        return dtStr
    }
    
    var tohhmma: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.dateFormat = "hh:mm a"
//        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date)
    }
    
    var toISOWithTime: Date {
        let datformt = DateFormatter()
        datformt.dateFormat = "MM/dd/yyyy H:m"
        let dt = datformt.date(from: self) ?? Date()
        return dt
    }
    
    var toFullDateYY: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: self) ?? Date()
        let dateFormattr = DateFormatter()
        dateFormattr.dateFormat = "MMM dd, yy h:mm a"
        return dateFormattr.string(from: date)
    }
    
    var dayy: Int {
        switch self {
        case "Monday":
            return 1
        case "Tuesday":
            return 2
        case "Wednesday":
            return 3
        case "Thursday":
            return 4
        case "Friday":
            return 5
        case "Saturday":
            return 6
        case "Sunday":
            return 7
        default:
            return 0
        }
    }
    
    //Mark:- Localize String varibale
    var localized: String {
        if currentBundle == nil {
            currentBundle = .main
        }
        return NSLocalizedString(self, tableName: nil, bundle: currentBundle, value: "", comment: "")
    }
    
    var fontWithSpacing: NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: CGFloat(2.0), range: NSRange(location: 0, length: attributedString.length))
        return attributedString
    }
    
    var age: String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let birthday = dateFormatter.date(from: self) ?? Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year!
        return "\(age)"
    }
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
