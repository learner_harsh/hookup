//
//  UIFont+Extension.swift
//  Hookup
//
//  Created by Mayur Boghani on 11/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

enum FontWeight: String {
    case regular = "Lato-Regular"
    case light = "Lato-Light"
    case thin = "Lato-Thin"
    case black = "Lato-Black"
    case bold = "Lato-Bold"
    case boldItalic = "Lato-BoldItalic"
}

extension UIFont {
    class func lato(ofSize size: CGFloat, weight: FontWeight) -> UIFont {
        return UIFont(name: weight.rawValue, size: size)!
    }
}
