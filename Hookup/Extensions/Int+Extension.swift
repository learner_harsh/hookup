//
//  Int+Extension.swift
//  BUZZ
//
//  Created by Mayur Boghani on 02/06/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Foundation

extension Int {
    static func toHourMin(hour: Int, min: Int) -> String {
        "\(hour.toDoubleDec()):\(min.toDoubleDec())"
    }
    
    func toDoubleDec() -> String {
        return self < 10 ? "0\(self)" : "\(self)"
    }
    
    var weekDay: String {
        switch self {
        case 1:
            return "Monday"
        case 2:
            return "Tuesday"
        case 3:
            return "Wednesday"
        case 4:
            return "Thursday"
        case 5:
            return "Friday"
        case 6:
            return "Saturday"
        case 7:
            return "Sunday"
        default:
            return ""
        }
    }
}
