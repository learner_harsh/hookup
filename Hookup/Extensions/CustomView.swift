//
//  CustomView.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

class CustomView: UIView {
    override func draw(_ rect: CGRect) {
        self.addShadow()
    }
}

class CustomRoundedView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 12.0
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        addCornerRadius()
    }

    
    func addCornerRadius() {
        layer.cornerRadius = cornerRadius
        self.addShadow()
    }
}

class CustomCurveView: UIView {
    
    var fillColor = ThemeColor.pink
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let curveHeight = bounds.height * 0.4
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: bounds.width, y: 0))
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.height - curveHeight))
        path.addQuadCurve(to: CGPoint(x: 0, y: bounds.height - curveHeight), controlPoint: CGPoint(x: bounds.width / 2, y: bounds.height))
//        path.addLine(to: CGPoint(x: 0, y: bounds.height - curveHeight))
        path.close()
        fillColor.setFill()
        path.fill()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}

