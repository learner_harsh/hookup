//
//  JsonEncode+Extension.swift
//  BUZZ
//
//  Created by Mayur Boghani on 03/06/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Foundation

struct JSON {
    static let encoder = JSONEncoder()
}
extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
    var array: [[String: Any]] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [[String: Any]] ?? []
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
