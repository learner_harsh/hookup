//
//  StorageManager.swift
//  BUZZ
//
//  Created by Mayur Boghani on 27/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import Foundation

enum ScreenType: Int {
    case login
    case profile
    case services
    case home
    case none
}

class StorageManager {
    static let shared = StorageManager()
    
    var userdefaults: UserDefaults {
        return UserDefaults.standard
    }
    
    var currentScreen: ScreenType {
        get { ScreenType(rawValue: userdefaults.integer(forKey: "currentScreen")) ?? .none }
        set { userdefaults.set(newValue.rawValue, forKey: "currentScreen") }
    }
    
    var isUserLogin: Bool {
        return authToken != "" && currentScreen == .home
    }
    
    var authToken: String {
        get { userdefaults.string(forKey: "authToken") ?? ""}
        set { userdefaults.set(newValue, forKey: "authToken") }
    }
    
    var userEmail: String {
        get { userdefaults.string(forKey: "userEmail") ?? ""}
        set { userdefaults.set(newValue, forKey: "userEmail") }
    }
    
    var userName: String {
        get { userdefaults.string(forKey: "userName") ?? ""}
        set { userdefaults.set(newValue, forKey: "userName") }
    }
    
    var userID: String {
        get { userdefaults.string(forKey: "userID") ?? ""}
        set { userdefaults.set(newValue, forKey: "userID") }
    }
    
//    var savedReqItems: [RequestItems] {
//        get {
//            if let dataencoded = userdefaults.data(forKey: "savedReqItems") {
//                let items = try? JSONDecoder().decode([RequestItems].self, from: dataencoded)
//                return items ?? []
//            } else {
//                return []
//            }
//        }
//        set {
//            if let data = try? JSONEncoder().encode(newValue) {
//                userdefaults.set(data, forKey: "savedReqItems")
//            }
//        }
//    }
}
