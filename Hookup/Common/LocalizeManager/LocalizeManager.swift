//
//  LocationManager.swift
//

import Foundation
import UIKit

var currentBundle: Bundle!

enum Language : String, Codable, CaseIterable {
    case english = "en"
    case arabic = "ar"
    case portugese = "pt-BR"

    var code : String {
        switch self {
        case .english:
            return "en"
        case .arabic:
            return "ar"
        case .portugese:
            return "pt-BR"
        }
    }
    
        var title : String {
            switch self {
            case .english:
                return LaunguageConstant.English
            case .arabic:
                return LaunguageConstant.Arabic
            case .portugese:
                return LaunguageConstant.Portugese
            }
        }
    
    static var count: Int{ return 3 }
}


class LocalizeManager {
    
    static var share = LocalizeManager()
    var selectedLanguage : Language = .english
    
     func changeLocalization(language:Language) {
        let defaults = UserDefaults.standard
        defaults.set(language.rawValue, forKey: "Language")
        UserDefaults.standard.synchronize()
    }
    
     func currentlocalization() -> String {
        if let savedLocale = UserDefaults.standard.object(forKey: "Language") as? String {
            return savedLocale
        }
        return Language.english.rawValue
    }
    
     func currentLanguage() -> String{
        
        switch currentlocalization() {
        case Language.english.rawValue:
            return LaunguageConstant.English.localized
        case Language.arabic.rawValue:
            return LaunguageConstant.Arabic.localized
        case Language.portugese.rawValue:
                return LaunguageConstant.Portugese.localized
            
        default:
            return ""
        }
    }
    
    func setLocalization(language : Language){
        
        if let path = Bundle.main.path(forResource: language.code, ofType: "lproj"), let bundle = Bundle(path: path) {
            let attribute : UISemanticContentAttribute = language == .arabic ? .forceRightToLeft : .forceLeftToRight
            UIView.appearance().semanticContentAttribute = attribute
            selectedLanguage = language
            currentBundle = bundle
        } else {
            currentBundle = .main
        }
    }
}
