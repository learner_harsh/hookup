//
//  Global.swift
//  BUZZ
//
//  Created by Mayur Boghani on 28/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

func showAlert(title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
}

func showAlertWithDismiss(title: String, message: String, completion: ((UIAlertAction) -> Void)?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: completion))
    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
}

func showAlertWithActions(title: String, message: String, action1Name: String, action2Name: String, completion: ((UIAlertAction) -> Void)?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: action1Name, style: .cancel, handler: nil))
    alert.addAction(UIAlertAction(title: action2Name, style: .destructive, handler: completion))
    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
}
