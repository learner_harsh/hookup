//
//  Constant.swift
//  GoJekUser
//
//  Created by apple on 18/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

enum Constant {
    static let signUpnow = "localize.signupnow"
    static let signIn = "localize.signin"
    static let flirtchat = "localize.flirtchat"
    static let welcome = "localize.welcome"
    static let signup = "localize.signup"
    static let username = "localize.username"
    static let email = "localize.email"
    static let phone = "localize.phone"
    static let password = "localize.password"
    static let forgotPass = "localize.forgotpass"
    static let alreadyaccount = "localize.alreadyaccount"
    static let profilelisting = "localize.profilelisting"
    static let browse = "localize.browse"
    static let servicesSettings = "localize.servicessetting"
    static let sendReq = "localize.sendreq"
    static let name = "localize.name"
    static let location = "localize.address"
    static let dob = "localize.dob"
    static let gender = "localize.gender"
    static let save = "localize.save"
    static let amount = "localize.amount"
    static let message = "localize.message"
    static let accept = "localize.accept"
    static let reject = "localize.reject"
    static let accepted = "localize.accepted"
    static let rejected = "localize.rejected"
    static let pending = "localize.pending"
    static let feedback = "localize.feedback"
}

enum LaunguageConstant {
    static let English = "English"
    static let Arabic = "Arabic"
    static let Portugese = "Portugese"
}

struct DateFormat {
    
    static let yyyymmddHHMMss = "yyyy-MM-dd HH:mm:ss"
    static let MMMddyyyyhhmmssa = "MMM dd, yyyy hh:mm:ss a"
    static let yyyymmddHHMMssa = "dd-MM-yyyy hh:mm a"
    static let hhmmddMMMyyyy = "hh:mm a - dd:MMM:yyyy"
    static let ddMMyyyyhhmma = "dd-MM-yyyy hh:mma"
    static let ddMMMyyyy = "dd MMM,yyyy"
    static let hhmma = "hh : mm a"
    static let ddMMyyyy = "dd/MM/yyyy"
    static let ddmmyyyy = "dd-MM-yyyy"
    static let dMMMhhmma = "d MMM, hh:mm a"
    static let ddMMMyy = "dd MMM yyyy, hh:mm a"
}
