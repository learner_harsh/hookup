//
//  RoundedButton.swift
//  Hookup
//
//  Created by Mayur Boghani on 10/03/21.
//  Copyright © 2021 Mayur Boghani. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = self.bounds.height / 2
        self.clipsToBounds = true
    }
    
}
