//
//  CustomLabel.swift
//  Buzz
//
//  Created by Mayur Boghani on 02/06/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {
    override func draw(_ rect: CGRect) {
        let insets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        super.drawText(in: rect.inset(by: insets))
    }
}
