//
//  AppDelegate.swift
//  BUZZ
//
//  Created by Mayur Boghani on 27/05/20.
//  Copyright © 2020 Mayur Boghani. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        setRootView()
//        FirebaseApp.configure()
        return true
    }

    // MARK: UISceneSession Lifecycle
    func setRootView() {
        if window != nil {
            if StorageManager.shared.isUserLogin {
                guard let vcMain = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as? MainScreenViewController else {return}
                guard let vcMenu = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController else {return}
                let sidemenu = SideMenuController(contentViewController: vcMain, menuViewController: vcMenu)
                sidemenu.navigationController?.isNavigationBarHidden = true
                window?.rootViewController = UINavigationController(rootViewController: sidemenu)
                window?.makeKeyAndVisible()
            } else {
                if StorageManager.shared.currentScreen == .profile {
                    guard let editvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController else {return}
                    window?.rootViewController = UINavigationController(rootViewController: editvc)
                    window?.makeKeyAndVisible()
                } else if StorageManager.shared.currentScreen == .services {
                    guard let serviceVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ServicesSettingsViewController") as? ServicesSettingsViewController else {return}
                    window?.rootViewController = UINavigationController(rootViewController: serviceVC)
                    window?.makeKeyAndVisible()
                } else {
                    guard let signUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController else {return}
                    window?.rootViewController = UINavigationController(rootViewController: signUpVC)
                    window?.makeKeyAndVisible()
                }
            }
        }
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

